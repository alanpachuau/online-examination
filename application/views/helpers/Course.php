<?php
class Zend_View_Helper_Course
{
	public function Course($course_id = null, $field = 'name')
	{
		if($course_id != null)
		{
			$course_model = new Model_Course();
			$course = $course_model->find($course_id)->current();

			if($field == 'name')
				return $course->course_name;
			elseif($field == 'duration')
				return $course->course_duration;
		}
		else return null;
	}
}