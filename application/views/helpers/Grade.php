<?php
class Zend_View_Helper_Grade
{
	public function Grade($percent = null)
	{
		if($percent != null)
		{
			$setting_model = new Model_Setting();
			$gradea = $setting_model->get_data('gradea');
			$gradebplus = $setting_model->get_data('gradebplus');
			$gradeb = $setting_model->get_data('gradeb');
			$gradecplus = $setting_model->get_data('gradecplus');
			$gradec = $setting_model->get_data('gradec');

			if($percent >= $gradea)
			{
				return "A";
			}
			elseif($percent >= $gradebplus)
			{
				return "B+";
			}
			elseif($percent >= $gradeb)
			{
				return "B";
			}
			elseif($percent >= $gradecplus)
			{
				return "C+";
			}
			elseif($percent >= $gradec)
			{
				return "C";
			}
			else return "Fail";
		}
		else return null;
	}
}