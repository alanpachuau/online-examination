<?php
class Model_User extends System_DbTable
{
    protected $_name = 'user';

    public function create($data, $type = 'administrator')
    {
        $salt = '';
        for($i=0; $i<32; $i++)
            $salt .= chr(rand(33, 126));

        $new_row = $this->createRow();
        $new_row->username = $data['username'];
        $new_row->password = md5($data['password'] . $salt);
        $new_row->password_salt = $salt;
        $new_row->type = $type;
        $new_row->created_at = new Zend_Db_Expr('NOW()');
        $new_row->updated_at = new Zend_Db_Expr('NOW()');
        $new_row->loggedin_at = new Zend_Db_Expr('NOW()');
        return $new_row->save();
    }

    public function change_password($password, $id)
    {
        if(is_numeric($id) && strlen($password))
        {
            $salt = '';
            for($i=0; $i<32; $i++)
                $salt .= chr(rand(33, 126));

            $row = $this->find($id)->current();
            if($row)
            {
                $row->password = md5($password . $salt);
                $row->password_salt = $salt;
                $row->updated_at = new Zend_Db_Expr('NOW()');
                return $row->save();
            }
        }
        else return false;
    }
}