<?php

class IndexController extends Zend_Controller_Action
{
	protected $auth;

	protected $userm;
	protected $examm;
	protected $studentm;
	protected $questionm;
	protected $examsessionm;

	public function init()
	{
		$this->userm = new Model_User();
		$this->examm = new Model_Exam();
		$this->studentm = new Model_Student();
		$this->questionm = new Model_Question();
		$this->examsessionm = new Model_ExamSession();

		$this->auth = Zend_Auth::getInstance();
    }

	public function indexAction()
    {
		if($this->auth->hasIdentity())
		{
			$current_user = $this->auth->getIdentity();
			if($current_user->type == 'student')
			{	
				$student = $this->studentm->fetchRow('user_id='.$current_user->id);
				// Get exam available today for current student
				$exam = $this->examm->exam_today($student->course_id);
				$exam_taken_today = $this->examsessionm->exam_taken_today($student->student_id);
				$this->view->current_user = $current_user;
				$this->view->student = $student;
				$this->view->exam = $exam;

				if($exam_taken_today)
				{
					$_SESSION['exam_session_id'] = $exam_taken_today->id;
					$this->view->exam_taken_today = $exam_taken_today;
					$this->view->exam = $this->examm->find($exam_taken_today->exam_id)->current();
					$this->view->questions = $this->examsessionm->questions($this->examsessionm->get_session());
					$this->view->examm = $this->examm;
					$this->view->questionm = $this->questionm;

					$this->renderScript('index/complete.phtml');
				}
				else if($exam && $exam->status == 'active')
				{
					$examsession = $this->examsessionm->check($student->student_id, $exam->exam_id, $exam->start_at, $exam->exam_duration);

					if($examsession)
						$_SESSION['exam_session_id'] = $examsession->id;
					else
					{
						// create session
						$exam_session_id = $this->examsessionm->prepare();
						if($exam_session_id)
						{
							$_SESSION['exam_session_id'] = $exam_session_id;

							$this->examm->update(array('status'=>'active'), 'exam_id = '.$exam->exam_id);
							$this->examsessionm->start($exam_session_id, $exam->exam_duration);
						}
					}

					$this->view->questions = $this->examsessionm->questions($this->examsessionm->get_session());
					$this->view->current_question = $this->examsessionm->current_question();

					$this->renderScript('index/start.phtml');
				}
				else
					$this->renderScript('index/student.phtml');
			}
			elseif($current_user->type == 'administrator')
			{
				$this->renderScript('index/admin.phtml');
			}
			elseif($current_user->type == 'faculty')
			{
				$this->renderScript('index/faculty.phtml');
			}
		}
	}
}



