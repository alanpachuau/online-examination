<?php
$configuration = array(
	'database' => array(
		'adapter' => 'pdo_mysql',
		'params'  => array(
			'host'     => 'localhost',
			'dbname'   => 'onlineexam',
			'username' => 'root',
			'password' => 'pass'
		)
	)
);