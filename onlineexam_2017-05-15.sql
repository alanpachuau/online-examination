# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.16)
# Database: onlineexam
# Generation Time: 2017-05-15 17:40:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table course
# ------------------------------------------------------------

DROP TABLE IF EXISTS `course`;

CREATE TABLE `course` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_name` varchar(300) NOT NULL DEFAULT '',
  `course_shortname` varchar(20) DEFAULT NULL,
  `course_duration` int(2) DEFAULT NULL COMMENT 'Number of semesters',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;

INSERT INTO `course` (`course_id`, `course_name`, `course_shortname`, `course_duration`, `created_at`, `updated_at`)
VALUES
	(1,'SOMETHING','ST',2,'2014-05-28 00:00:00','2014-05-28 00:00:00');

/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exam
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exam`;

CREATE TABLE `exam` (
  `exam_id` int(11) NOT NULL AUTO_INCREMENT,
  `course_id` int(11) NOT NULL,
  `exam_name` varchar(35) NOT NULL DEFAULT '',
  `exam_duration` int(2) NOT NULL,
  `start_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `no_of_question` int(50) NOT NULL,
  `no_of_student` int(25) NOT NULL,
  `faculty_id` int(11) NOT NULL COMMENT 'Invigilator',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` varchar(10) NOT NULL DEFAULT 'inactive',
  PRIMARY KEY (`exam_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `exam` WRITE;
/*!40000 ALTER TABLE `exam` DISABLE KEYS */;

INSERT INTO `exam` (`exam_id`, `course_id`, `exam_name`, `exam_duration`, `start_at`, `end_at`, `no_of_question`, `no_of_student`, `faculty_id`, `created_at`, `updated_at`, `status`)
VALUES
	(1,1,'BCA entrance Exam 2014',3,'2016-06-16 11:10:00','2016-06-16 11:13:00',2,3,2,'2014-05-28 22:53:18','2016-06-16 11:08:17','active');

/*!40000 ALTER TABLE `exam` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table examsession
# ------------------------------------------------------------

DROP TABLE IF EXISTS `examsession`;

CREATE TABLE `examsession` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `questions` text,
  `current_question` int(4) DEFAULT NULL,
  `start_at` datetime DEFAULT NULL,
  `end_at` datetime DEFAULT NULL,
  `time_elapsed` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `examsession` WRITE;
/*!40000 ALTER TABLE `examsession` DISABLE KEYS */;

INSERT INTO `examsession` (`id`, `student_id`, `exam_id`, `course_id`, `questions`, `current_question`, `start_at`, `end_at`, `time_elapsed`, `created_at`, `updated_at`, `status`)
VALUES
	(1,6,1,1,'a:2:{i:0;a:2:{s:1:\"q\";s:1:\"3\";s:1:\"a\";s:4:\"opt4\";}i:1;a:2:{s:1:\"q\";s:1:\"1\";s:1:\"a\";s:4:\"opt4\";}}',2,'2014-05-30 00:02:00','2014-05-30 00:05:00',0,'2014-05-30','2014-05-30','completed'),
	(2,5,1,1,'a:4:{i:0;a:2:{s:1:\"q\";s:1:\"1\";s:1:\"a\";s:4:\"opt1\";}i:1;a:2:{s:1:\"q\";s:1:\"4\";s:1:\"a\";s:4:\"opt1\";}i:2;a:2:{s:1:\"q\";s:1:\"2\";s:1:\"a\";s:4:\"opt3\";}i:3;a:2:{s:1:\"q\";s:1:\"3\";s:1:\"a\";s:4:\"opt2\";}}',4,'2016-06-16 11:08:25','2016-06-16 11:11:25',0,'2016-06-16','2016-06-16','completed');

/*!40000 ALTER TABLE `examsession` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table faculty
# ------------------------------------------------------------

DROP TABLE IF EXISTS `faculty`;

CREATE TABLE `faculty` (
  `faculty_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `faculty_name` varchar(250) NOT NULL DEFAULT '',
  `faculty_designation` varchar(250) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`faculty_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `faculty` WRITE;
/*!40000 ALTER TABLE `faculty` DISABLE KEYS */;

INSERT INTO `faculty` (`faculty_id`, `user_id`, `faculty_name`, `faculty_designation`, `created_at`, `updated_at`)
VALUES
	(2,19,'Sachin Dhar Dwidevi','AE','2014-05-20 02:26:03','2014-05-20 02:26:03');

/*!40000 ALTER TABLE `faculty` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table question
# ------------------------------------------------------------

DROP TABLE IF EXISTS `question`;

CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) NOT NULL,
  `question_type` varchar(30) NOT NULL DEFAULT '',
  `question` varchar(200) NOT NULL,
  `opt1` varchar(50) NOT NULL,
  `opt2` varchar(50) NOT NULL,
  `opt3` varchar(50) NOT NULL,
  `opt4` varchar(50) NOT NULL,
  `ans` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;

INSERT INTO `question` (`question_id`, `exam_id`, `question_type`, `question`, `opt1`, `opt2`, `opt3`, `opt4`, `ans`, `created_at`, `updated_at`)
VALUES
	(1,1,'multiple','What comes after 2 ?','1','2','3','4','opt3','2014-05-20 02:29:10','2014-05-20 02:29:10'),
	(2,1,'multiple','What comes after G?','G','H','I','J','opt2','2014-05-20 02:29:32','2014-05-20 02:29:32'),
	(3,1,'multiple','8 bits is a __________','byte','bytes','bits','None of the above','opt1','2014-05-20 02:30:22','2014-05-20 02:30:22'),
	(4,1,'multiple','If  log&#x2093;( &#x215B;)= - 3/2 , then x is equal to  ','-4','&frac14; ','4','10','opt2','2014-05-27 14:34:04','2014-05-27 14:34:04');

/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table result
# ------------------------------------------------------------

DROP TABLE IF EXISTS `result`;

CREATE TABLE `result` (
  `result_id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`result_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table setting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `key` varchar(20) NOT NULL DEFAULT '',
  `data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `setting` WRITE;
/*!40000 ALTER TABLE `setting` DISABLE KEYS */;

INSERT INTO `setting` (`key`, `data`)
VALUES
	('correct_mark','1'),
	('gradea','80'),
	('gradeb','60'),
	('gradebplus','70'),
	('gradec','40'),
	('gradecplus','50'),
	('incorrect_mark','0');

/*!40000 ALTER TABLE `setting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table student
# ------------------------------------------------------------

DROP TABLE IF EXISTS `student`;

CREATE TABLE `student` (
  `student_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `student_name` varchar(250) DEFAULT NULL,
  `roll_no` varchar(250) NOT NULL DEFAULT '',
  `parent_name` varchar(250) NOT NULL DEFAULT '',
  `dob` date NOT NULL,
  `email_id` varchar(250) NOT NULL DEFAULT '',
  `contact_no` varchar(20) NOT NULL DEFAULT '',
  `address` text NOT NULL,
  `centre` varchar(250) NOT NULL DEFAULT '',
  `institution` varchar(250) NOT NULL DEFAULT '',
  `sex` varchar(15) NOT NULL,
  `nationality` varchar(250) NOT NULL DEFAULT '',
  `passport` varchar(400) NOT NULL,
  `signature` varchar(400) NOT NULL,
  `date_of_registration` date NOT NULL,
  `category` varchar(250) NOT NULL DEFAULT '',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;

INSERT INTO `student` (`student_id`, `user_id`, `course_id`, `student_name`, `roll_no`, `parent_name`, `dob`, `email_id`, `contact_no`, `address`, `centre`, `institution`, `sex`, `nationality`, `passport`, `signature`, `date_of_registration`, `category`, `created_at`, `updated_at`)
VALUES
	(5,16,1,'Bojen Singh','BCA001','Deba','1980-07-01','bo','ejn@yahoo.co.in','Kulikawn','Aizawl','NIELIT','Male','Indian','','','2014-05-20','General','2014-05-20 02:16:21','2014-05-20 02:16:21'),
	(6,17,1,'Lalrempuia','BCA002','TP Singh','1977-01-01','rempuia@gmail.com','2342342342','NIELIT Zuangtui','Aizawl','NIELIT','Male','Indian','','','2014-05-20','General','2014-05-20 02:17:19','2014-05-20 02:17:19'),
	(7,18,1,'Dengchhunga Renthlei','BCA003','Dengchhunga Rentheli','1975-01-01','rrr@gmail.com','32143212','Ramhlun','Aizawl','NIELIT','Male','Indian','','','2014-05-20','General','2014-05-20 02:18:58','2014-05-20 02:18:58'),
	(8,20,1,'Vanlalhmangaiha','BCA007','Lalduhawma','1980-06-10','hjh@gmail.com','8746839','jhdj','Aizawl','Aizawl','Male','Indian','/uploads/students/8-passport.jpg','/uploads/students/8-signature.png','2014-05-27','ST','2014-05-27 14:43:04','2014-05-27 14:43:04'),
	(9,21,1,'Thlaa','BCA008','ass','2014-05-05','ewqw@fds.com','34345','ghfgh','Aizawl','Aizawl','Male','Indian','/uploads/students/9-passport.jpg','/uploads/students/9-signature.png','2014-05-27','ST','2014-05-27 16:59:13','2014-05-27 16:59:13');

/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(300) NOT NULL,
  `password_salt` varchar(400) NOT NULL,
  `type` enum('student','administrator','faculty') NOT NULL DEFAULT 'student',
  `loggedin_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`id`, `username`, `password`, `password_salt`, `type`, `loggedin_at`, `created_at`, `updated_at`)
VALUES
	(1,'admin','f4f0251b3b97630c3bff2514612f3040','3o:o\"G2Xh8aq\\4g5D[oi2Ax+kcN@HUM$','administrator','2013-04-30 19:01:02','2013-04-30 19:01:02','2014-05-20 01:46:44'),
	(15,'van','bc477470ca08347e85ffd00e7ab5f45b','ce8d96d579d389e783f95b3772785783ea1a9854','administrator','2014-05-20 11:57:14','2014-05-20 11:57:14','2014-05-20 11:57:14'),
	(16,'BCA001','b34c8e1cf18735b0ec9f0e1d7855892c','TQD[5W<8HDbsL*VaxiO1:,oYT:[hnr_C','student','2014-05-20 02:16:21','2014-05-20 02:16:21','2016-06-16 11:07:41'),
	(17,'BCA002','c53fea344aa913dd31d4031db00144eb','5$A-\"3|75-bA\'aN%;2kK&PY--{D3eG(7','student','2014-05-20 02:17:19','2014-05-20 02:17:19','2014-05-20 02:17:19'),
	(18,'BCA003','26718da5154b9f84511589c6ea354010','T33Bn/a,:\\\"^*N/R$$Wp=.84[/\\b3ubv','student','2014-05-20 02:18:58','2014-05-20 02:18:58','2014-05-20 02:18:58'),
	(19,'sachin','362a3f58a22b34b2d7af563570c9187a','G7?j8CbjE\\$/%u=QxO}x9:$\"iUxq_5\\%','faculty','2014-05-20 02:26:03','2014-05-20 02:26:03','2014-05-20 02:26:03'),
	(20,'BCA007','58a9e5897e4f3228b7ccb7aec522cf9f','!%+4v38r^F-\'x!7v|pR{O9M@e+$[Dfbw','student','2014-05-27 14:43:04','2014-05-27 14:43:04','2014-05-27 14:43:04'),
	(21,'BCA008','bc113007f40f891923be69d2876852f5','}5*<hLt40?eey.*y;i|IwTm>\'=X0LFh)','student','2014-05-27 16:59:12','2014-05-27 16:59:12','2014-05-27 16:59:12');

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
