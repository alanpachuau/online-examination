<?php
class System_AclHelper
{
	public $acl;
	
	public function __construct()
	{
		$this->acl = new Zend_Acl();
	}
	
	public function setRoles()
	{
		$this->acl->addRole(new Zend_Acl_Role('public'));
		$this->acl->addRole(new Zend_Acl_Role('student'));
		$this->acl->addRole(new Zend_Acl_Role('faculty'));
		$this->acl->addRole(new Zend_Acl_Role('administrator'));
	}
	
	public function setResources()
	{
		$this->acl->addResource(new Zend_Acl_Resource("default"));
		
		$this->acl->addResource(new Zend_Acl_Resource("default:index:index"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:change-password:index"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:error:error"), "default");
		
		$this->acl->addResource(new Zend_Acl_Resource("default:auth:login"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:auth:logout"), "default");
		
		$this->acl->addResource(new Zend_Acl_Resource("default:course:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:course:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:course:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:course:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:course:edit"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:exam:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:exam:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:exam:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:exam:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:exam:edit"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:admin:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:admin:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:admin:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:admin:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:admin:reset-password"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:edit"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:faculty:reset-password"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:student:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:student:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:student:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:student:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:student:edit"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:student:reset-password"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:question:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:question:list"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:question:add"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:question:delete"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:question:edit"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:index"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:check-exam-start-at"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:check-exam-end-at"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:prepare-session"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:question"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:set-current-question"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:save-answer"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:get-questions"), "default");
		$this->acl->addResource(new Zend_Acl_Resource("default:ajax:evaluate"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:setting:index"), "default");

		$this->acl->addResource(new Zend_Acl_Resource("default:result:index"), "default");
		
		$this->acl->addResource(new Zend_Acl_Resource("default:about:index"), "default");
	}
	
	public function setPrivileges()
	{
		$this->acl->deny(array(
			'public',
			'faculty',
			'student',
			'administrator'
			), null);

		$this->acl->allow('student', array(
			'default:error:error',
			'default:index:index',
			'default:ajax:index',
			'default:ajax:check-exam-start-at',
			'default:ajax:check-exam-end-at',
			'default:ajax:prepare-session',
			'default:ajax:question',
			'default:ajax:set-current-question',
			'default:ajax:save-answer',
			'default:ajax:get-questions',
			'default:ajax:evaluate',
			'default:result:index'
			));
		$this->acl->allow('faculty', array(
			'default:question:index',
			'default:question:list',
			'default:question:add',
			'default:question:delete',
			'default:question:edit',
			'default:student:index',
			'default:student:list',
			'default:student:add',
			'default:student:delete',
			'default:student:edit',
			'default:student:reset-password',
			'default:about:index'
			));
		$this->acl->allow('administrator', null);
		$this->acl->allow(null, array(
			'default:auth:login',
			'default:auth:logout'
			));
	}
	
	public function setAcl()
	{
		Zend_Registry::set('acl', $this->acl);
	}
}