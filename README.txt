HOW TO SETUP
============

1. Install wampserver.

2. Configuration of wampserver:
	
	a) C:\wamp\bin\apache\apache.x.x.x\conf\extra\apache-vhosts.conf
	Virtualhost entry

	b) apache module mod_rewrite enabled

3. Edit system hosts file
	C:\Windows\System32\drivers\etc\hosts
	Add => 127.0.0.1 onlineexam.final

4. Create site folder in C:\wamp\www\ and put your project files here

5. Make sure that Zend library exists in library folder

6. Create database and Import the database export.

7. Change database configuration in config\db.php

8. That's all
